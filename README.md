# Docker Data Science
### Environment
Edit the environment.yml file to include/remove any conda or pip dependecies.
### Build the Docker Image
`cd path/to/repo`  
`docker build -t flowlab .`
### Run the Container
`docker-compose run --rm --service-ports container`  
Acceess Jupyter Lab or MLFlow by copying and pasting the URLs into your browser
