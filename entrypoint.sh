#!/bin/bash

su -c "tmux new-session -d -s mlflow"
su -c "tmux send -t mlflow \"cd code\" ENTER"
su -c "tmux send -t mlflow \"conda activate flowlab\" ENTER"
su -c "tmux send -t mlflow \"mlflow ui --backend-store-uri file:///code/mlruns --host 0.0.0.0\" ENTER"

su -c "tmux new-session -d -s jupyterlab"
su -c "tmux send -t jupyterlab \"cd code\" ENTER"
su -c "tmux send -t jupyterlab \"conda activate flowlab\" ENTER"
su -c "tmux send -t jupyterlab \"jupyter lab --ip=0.0.0.0 --port=8888 --allow-root --no-browser\" ENTER"

sleep 5

printf "\n"
echo '    ________              __         __  ';
echo '   / ____/ /___ _      __/ /  ____ _/ /_ ';
echo '  / /_  / / __ \ | /| / / /  / __ \`/ __\';
echo ' / __/ / / /_/ / |/ |/ / /__/ /_/ / /_/ /';
echo '/_/   /_/\____/|__/|__/_____|__,_/_.___/ ';
echo '                                         ';


laburi=$(conda run -n flowlab jupyter notebook list | sed -e 's/0.0.0.0/127.0.0.1/g' | grep http)
printf "\nJupyter Lab: $laburi \n"
printf "MLFlow: localhost:5000\n\n\n"

/bin/bash